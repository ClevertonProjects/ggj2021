using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class SkillActivationSensor : MonoBehaviour
{
    [SerializeField] private UiGroupFader canvasFader;                                  //!< Details window fading effect.
    [SerializeField] private TextMeshProUGUI txtStrengthValue;                          //!< Skill strength comparisson text.
    [SerializeField] private TextMeshProUGUI txtHealthValue;                            //!< Skill health comparisson text.

    [SerializeField] private Image imgStrengthStatus;                                   //!< Strength comparisson status icon.
    [SerializeField] private Image imgHealthStatus;                                     //!< Health comparisson status icon.

    [SerializeField] private Sprite statusIncreaseIcon;                                 //!< Icon indicating status increase.
    [SerializeField] private Sprite statusEquivalentIcon;                               //!< Icon indicating no change in the attribute.
    [SerializeField] private Sprite statusDecreaseIcon;                                 //!< Icon indicating status decrease.

    [SerializeField] private SpriteRenderer boxSpriteRenderer;                          //!< Skill box renderer.

    [SerializeField] private Skills skill;                                              //!< Actual skill in the box.

    [SerializeField] private AudioSource confirmAudio;                                  

    /// Sequence to update the box skill details.
    private IEnumerator BoxUpdateSequence (Sprite newSkillBoxSprite)
    {
        GetComponent<Collider2D>().enabled = false;
        canvasFader.FadeOut();

        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float proportion = 0f;
        float elapsedTime = 0f;

        while (proportion < 1f)
        {
            boxSpriteRenderer.color = Color.Lerp(Color.white, Color.clear, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / 1f;
        }

        boxSpriteRenderer.sprite = newSkillBoxSprite;

        proportion = 0f;
        elapsedTime = 0f;

        while (proportion < 1f)
        {
            boxSpriteRenderer.color = Color.Lerp(Color.clear, Color.white, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / 1f;
        }

        boxSpriteRenderer.color = Color.white;

        GetComponent<Collider2D>().enabled = true;
    }

    private void OnChangeConfirmed ()
    {
        confirmAudio.Play();
        PlayerController player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
        skill = player.SetSkill(skill);

        StartCoroutine(BoxUpdateSequence(skill.GetSkillBoxSprite));
    }

    /// Compares the actual player's status with the 
    private void CompareStatus (PlayerController player)
    {
        int strengthComparisson = skill.GetDamageValue - player.GetSkillDamageStatus;

        if (strengthComparisson > 0)
        {
            imgStrengthStatus.sprite = statusIncreaseIcon;
            txtStrengthValue.text = strengthComparisson.ToString(); 
        }
        else if (strengthComparisson < 0)
        {
            imgStrengthStatus.sprite = statusDecreaseIcon;
            txtStrengthValue.text = strengthComparisson.ToString();
        }
        else
        {
            imgStrengthStatus.sprite = statusEquivalentIcon;
            txtStrengthValue.text = "=";
        }

        int healthComparisson = skill.GetHeathValue - player.GetSkillHealthStatus;

        if (healthComparisson > 0)
        {
            imgHealthStatus.sprite = statusIncreaseIcon;
            txtHealthValue.text = healthComparisson.ToString();
        }
        else if (healthComparisson < 0)
        {
            imgHealthStatus.sprite = statusDecreaseIcon;
            txtHealthValue.text = healthComparisson.ToString();
        }
        else
        {
            imgHealthStatus.sprite = statusEquivalentIcon;
            txtHealthValue.text = "=";
        }
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            PlayerController player = collision.GetComponent<PlayerController>();

            player.SetInExchangeArea(OnChangeConfirmed);
            CompareStatus(player);
            canvasFader.FadeIn();
        }
    }

    private void OnTriggerExit2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            collision.GetComponent<PlayerController>().SetOutOfExchangeArea();
            canvasFader.FadeOut();
        }
    }
}
