using UnityEngine;
using UnityEngine.InputSystem;

public class TutorialController : MonoBehaviour
{
    [SerializeField] private AudioSource startButtonAudio;

    private bool ignoreInputs;

    /// <summary>
    /// Event called when the action input is pressed.
    /// </summary>    
    public void OnActionInput(InputAction.CallbackContext context)
    {
        if (context.performed && !ignoreInputs)
        {
            ignoreInputs = true;
            startButtonAudio.Play();
            SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
            fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToGameScene);
        }
    }
}
