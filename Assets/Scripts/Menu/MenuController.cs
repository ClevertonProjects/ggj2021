using UnityEngine;
using UnityEngine.InputSystem;

public class MenuController : MonoBehaviour
{
    [SerializeField] private ButtonTransitions[] menuButtons;                           //!< Menu buttons transition controller.
    private int highlightedOptionIndex;                                                 //!< Index of the highlighted menu option.
    [SerializeField] private PanelMovementAnim buttonsPanel;                            //!< Options buttons panel animation.
    [SerializeField] private PanelMovementAnim creditsPanel;                            //!< Credits panel animation.

    [SerializeField] private float moveSensitivityDelay;                                //!< Movement inputs reading delay.

    [SerializeField] private AudioSource startButtonAudio;
    [SerializeField] private AudioSource regularButtonAudio;

    private bool isCreditsOpen;                                                         //!< Check if the credits panel is opened.
    private bool isInputLocked = true;                                                  //!< Locks the inputs.

    private void Start()
    {
        Cursor.visible = false;
    }

    /// <summary>
    /// Initializes the menu options panel.
    /// </summary>
    public void ShowMenuOptions()
    {
        menuButtons[highlightedOptionIndex].PlayHighlightAnimation();

        for (int i = 1; i < menuButtons.Length; i++)
        {
            menuButtons[i].PlayDeselectAnimation();
        }

        buttonsPanel.MoveIn();
        isInputLocked = false;
    }

    // <summary>
    /// Event called when any movement input is pressed.
    /// </summary>    
    public void OnMoveInput(InputAction.CallbackContext context)
    {
        Vector2 input = context.ReadValue<Vector2>();
        int roundedY = Mathf.RoundToInt(input.y);

        if (context.performed && !isInputLocked && roundedY != 0)
        {
            if (roundedY > 0 && highlightedOptionIndex > 0)
            {             
                menuButtons[highlightedOptionIndex].PlayDeselectAnimation();
                highlightedOptionIndex--;
                menuButtons[highlightedOptionIndex].PlayHighlightAnimation();
            }
            else if (roundedY < 0 && highlightedOptionIndex < menuButtons.Length - 1)
            {                
                menuButtons[highlightedOptionIndex].PlayDeselectAnimation();
                highlightedOptionIndex++;
                menuButtons[highlightedOptionIndex].PlayHighlightAnimation();
            }            
        }
    }

    /// <summary>
    /// Event called when the action input is pressed.
    /// </summary>    
    public void OnActionInput(InputAction.CallbackContext context)
    {
        if (context.performed && !isInputLocked)
        {
            if (isCreditsOpen)
            {
                regularButtonAudio.Play();
                menuButtons[highlightedOptionIndex].PlayHighlightAnimation();
                isCreditsOpen = false;
                creditsPanel.MoveOut();
                buttonsPanel.MoveIn();
            }
            else
            {
                menuButtons[highlightedOptionIndex].PlayClickAnimation();

                if (highlightedOptionIndex == 0)
                {
                    startButtonAudio.Play();
                    isInputLocked = true;
                    SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
                    fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToTutorialScene);
                }
                else if (highlightedOptionIndex == 1)
                {
                    regularButtonAudio.Play();
                    isCreditsOpen = true;
                    buttonsPanel.MoveOut();
                    creditsPanel.MoveIn();
                }
                else
                {
                    regularButtonAudio.Play();
                    isInputLocked = true;
                    Application.Quit();
                }
            }
        }
    }
}
