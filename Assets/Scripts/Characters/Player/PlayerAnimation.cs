using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;                                         //!< Player's Animator controller.

    private readonly int IsDashing = Animator.StringToHash("IsDashing");            //!< Hash of the animator's isDashing skill parameter.
    private readonly int IsFacingUp = Animator.StringToHash("IsFacingUp");          //!< Hash of animator's isFacingUp direction parameter.
    private readonly int Horizontal = Animator.StringToHash("Horizontal");          //!< Hash of animator's horizontal movement parameter.
    private readonly int Vertical = Animator.StringToHash("Vertical");              //!< Hash of animator's vertical movement parameter.
    private readonly int IsHit = Animator.StringToHash("IsHit");                    //!< Hash of animator's isHit parameter.
    private readonly int IsDead = Animator.StringToHash("IsDead");                  //!< Hash of animator's isDead parameter.
    private readonly int AttackFront = Animator.StringToHash("AttackFront");        //!< Hash of animator's attackFront parameter.
    private readonly int AttackUp = Animator.StringToHash("AttackUp");              //!< Hash of animator's attackUp parameter.
    private readonly int AttackDown = Animator.StringToHash("AttackDown");          //!< Hash of animator's attackDown parameter.
        
    /// <summary>
    /// Sets the animation moving parameter values.
    /// </summary>    
    public void SetMovingValues (Vector2 direction, Vector2 previousDir, FacingDirection facingDirection)
    {
        int roundedX = Mathf.RoundToInt(direction.x);
        int roundedY = Mathf.RoundToInt(direction.y);

        if (facingDirection == FacingDirection.Up)
        {
            anim.SetBool(IsFacingUp, true);
        }
        else
        {
            anim.SetBool(IsFacingUp, false);
        }

        anim.SetInteger(Vertical, roundedY);
        anim.SetInteger(Horizontal, roundedX);
    }

    /// <summary>
    /// Sets the activation state of the dash animation.
    /// </summary>    
    public void SetDashAnimationState (bool isOn)
    {
        anim.SetBool(IsDashing, isOn);
    }

    /// <summary>
    /// Plays the attack animation.
    /// </summary>
    public void PlayAttackAnimation (FacingDirection facingDir)
    {
        switch (facingDir)
        {
            case FacingDirection.Up:
                anim.SetTrigger(AttackUp);
                break;            

            case FacingDirection.Down:
                anim.SetTrigger(AttackDown);
                break;

            case FacingDirection.Left:
            case FacingDirection.Right:
            default:
                anim.SetTrigger(AttackFront);
                break;
        }        
    }
   
    /// <summary>
    /// Plays the hit animation.
    /// </summary>
    public void PlayHitAnimation ()
    {
        anim.SetTrigger(IsHit);
    }

    /// Returns the timescale to default.
    private void ResumeTimeScale ()
    {
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Plays the death animation.
    /// </summary>
    public void PlayDeathAnimation ()
    {
        anim.SetTrigger(IsDead);
        Time.timeScale = 0.2f;

        DelayedCall.Invoke(ResumeTimeScale, 0.6f, this);
    }
}
