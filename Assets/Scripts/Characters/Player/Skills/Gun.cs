using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Custom/Skills/Gun", order = ScriptableOrder.SkillGun)]

public class Gun : Skills
{
    [SerializeField] private float effectSpawnDist;                                     //!< Distance from the character to spawn the skill effect.

    public override void UseSkill(PlayerController player)
    {
        if (isReady)
        {
            isReady = false;
            player.Shoot(effectPrefab, damageValue, effectSpawnDist);
            DelayedCall.Invoke(player.UnlockController, duration, player);
            player.StartCoroutine(ProcessCooldown());
        }
    }
}
