using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Custom/Skills/Dash", order = ScriptableOrder.SkillDash)]
public class Dash : Skills
{
    [SerializeField] private float dashSpeed;                                       //!< Movement speed while dashing.        
    [SerializeField] private Sprite upwardDashSprite;                               //!< Sprite of the character dashing upwardly.

    public override void UseSkill (PlayerController player)
    {
        if (isReady)
        {
            isReady = false;
            player.ApplyDashSpeed(dashSpeed, damageValue, effectPrefab, upwardDashSprite);
            DelayedCall.Invoke(player.ReturnToNormalSpeed, duration, player);
            player.StartCoroutine(ProcessCooldown());
        }
    }
}
