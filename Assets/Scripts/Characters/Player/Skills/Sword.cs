using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Custom/Skills/Sword", order = ScriptableOrder.SkillSword)]
public class Sword : Skills
{
    [SerializeField] private float effectSpawnDist;                                     //!< Distance from the character to spawn the skill effect.

    public override void UseSkill (PlayerController player)
    {
        if (isReady)
        {
            isReady = false;
            player.PerformeSlash(effectPrefab, damageValue, effectSpawnDist);
            DelayedCall.Invoke(player.UnlockController, duration, player);
            player.StartCoroutine(ProcessCooldown());
        }
    }
}
