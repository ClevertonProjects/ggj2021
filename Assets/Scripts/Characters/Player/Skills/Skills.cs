using System.Collections;
using UnityEngine;

public class Skills : ScriptableObject
{
    [SerializeField] protected float duration;                              //!< Amount of time before the player can control the character again.
    [SerializeField] protected float cooldown;                              //!< Cooldown to activate the skill after use.    
    [SerializeField] protected int healthValue;                             //!< Maximum player's health when this skill is active.
    [SerializeField] protected int damageValue;                             //!< Skill damage.
    protected bool isReady;                                                 //!< Check if the skill is ready to use.

    [SerializeField] protected GameObject effectPrefab;                     //!< Skill effect prefab.    
    [SerializeField] private Sprite skillBoxSprite;                         //!< Skill's collect box sprite.
    [SerializeField] private Sprite skillUiIcon;                            //!< Skill icon sprite.

    private void OnEnable()
    {
        isReady = true;
    }

    #region Properties

    public Sprite GetSkillBoxSprite
    {
        get { return skillBoxSprite; }
    }

    public int GetHeathValue
    {
        get { return healthValue; }
    }

    public int GetDamageValue
    {
        get { return damageValue; }
    }

    public float GetCooldown
    {
        get { return cooldown; }
    }

    public bool IsReady
    {
        get { return isReady; }
    }

    public Sprite SkillUiIcon
    { 
        get { return skillUiIcon; }
    }

    #endregion

    /// Counts the cooldown time to activate the skill.
    protected IEnumerator ProcessCooldown ()
    {
        WaitForSeconds delay = new WaitForSeconds(cooldown);
        yield return delay;
        isReady = true;
    }

    /// <summary>
    /// Trigger the skill effect.
    /// </summary>
    public virtual void UseSkill (PlayerController player)
    {

    }
}
