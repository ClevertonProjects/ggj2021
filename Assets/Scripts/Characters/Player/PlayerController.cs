using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed;                                   //!< Character's movement speed.
    private float actualSpeed;                                                  //!< Actual movement speed.
    [SerializeField] private float hitFreezeTime;                               //!< Amount of time, freezed when hit.
    [SerializeField] private Skills skill;                                      //!< Character's active skill.
    [SerializeField] private PlayerAnimation playerAnim;                        //!< Class that controls the character's animations.
    [SerializeField] private SkillCoodownEffect cooldownEffect;                 //!< Skill cooldown effect.
    [SerializeField] private ActiveIconTransition skillIconTransition;          //!< Controls the skill UI icons transition.

    private FacingDirection facingDir;                                          //!< Actual direction that the character is looking to.

    private bool isInvincible;                                                  //!< Check if the character is invincible to damage.
    private bool isInputLocked;                                                 //!< Check if the player's input update is locked.     
    private bool isInSkillExchangeArea;                                         //!< Check if the player is inside an skill exchange area.

    private Vector2 inputValues;                                                //!< Player input values.
    private Vector2 previousInputs;                                             //!< Previous input values.
   
    [SerializeField] private AudioSource hitAudio;
    [SerializeField] private AudioSource deathAudio;

    [Header("Collision")]
    [SerializeField] private float maxRayDistance;                              //!< Maximum raycast distance to check for walls ahead.
    [SerializeField] private Vector2 characterSize;                             //!< Character's collision size.    

    private Transform myTransform;                                              //!< Character's Transform component.

    private ActionCallbacks.Callback onSkillExchange;                           //!< Event called when an skill exchange is confirmed.

    #region Properties

    public bool IsInvincible
    {
        get { return isInvincible; }
    }

    public int GetSkillHealthStatus
    { 
        get { return skill.GetHeathValue; }
    }

    public int GetSkillDamageStatus
    {
        get { return skill.GetDamageValue; }
    }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        actualSpeed = moveSpeed;
        myTransform = transform;
        facingDir = FacingDirection.Right;

        GetComponent<PlayerHealth>().SetInitialHealth(OnDeath);
    }

    /// Sets the character's facing direction.
    private void SetFacingDirection (Vector2 actualDir, Vector2 previousDir)
    {   
        int roundedX = Mathf.RoundToInt(inputValues.x);
        int roundedY = Mathf.RoundToInt(inputValues.y);
        int roundedPreviousX = Mathf.RoundToInt(previousDir.x);
        int roundedPreviousY = Mathf.RoundToInt(previousDir.y);

        if (roundedX > 0 && (roundedY == 0 || (roundedY != 0 && roundedPreviousY == 0)))
        {
            facingDir = FacingDirection.Right;
        }
        else if (roundedX < 0 && (roundedY == 0 || (roundedY != 0 && roundedPreviousY == 0)))
        {
            facingDir = FacingDirection.Left;
        }
        else if (roundedY > 0 && (roundedX == 0 || (roundedX != 0 && roundedPreviousX == 0)))
        {
            facingDir = FacingDirection.Up;
        }
        else if (roundedY < 0 && (roundedX == 0 || (roundedX != 0 && roundedPreviousX == 0)))
        {
            facingDir = FacingDirection.Down;
        }        
    }

    /// Flips the transform accordingly to the moving direction.
    private void Flip ()
    {
        int dir = Mathf.RoundToInt(inputValues.x);        

        if (dir != 0)
        {         
            myTransform.localScale = new Vector3(dir, 1f, 1f);
        }
    }

    /// Applies the character's movement.
    private void Move ()
    {
        if (IsByWall())
        {
            inputValues = Vector2.zero;
            playerAnim.SetMovingValues(inputValues, previousInputs, facingDir);
            previousInputs = inputValues;
        }
        
        Vector3 moveDir = new Vector3(inputValues.x, inputValues.y);
        myTransform.position += moveDir * actualSpeed * Time.deltaTime;        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }       

    #region Inputs

    /// <summary>
    /// Event called when any movement input is pressed.
    /// </summary>    
    public void OnMoveInput (InputAction.CallbackContext context)
    {
        Vector2 newInput = context.ReadValue<Vector2>();

        if (!isInputLocked)
        {
            inputValues = newInput;
            SetFacingDirection(inputValues, previousInputs);
            playerAnim.SetMovingValues(newInput, previousInputs, facingDir);
            Flip();            
        }

        previousInputs = context.ReadValue<Vector2>();
    }

    /// <summary>
    /// Event called when the skill input is pressed.
    /// </summary>    
    public void OnSkillInput (InputAction.CallbackContext context)
    {
        if (context.performed && !isInputLocked)
        {
            if (isInSkillExchangeArea)
            {
                onSkillExchange?.Invoke();
            }
            else if (skill != null)
            {
                if (skill.IsReady)
                {
                    cooldownEffect.TriggerCooldown(skill.GetCooldown);
                }

                skill.UseSkill(this);                
            }
            else
            {
                Debug.Log("No skill asigned");
            }
        }
    }

    #endregion

    #region Collision check

    /// Check if the character has a wall in front of him.
    private bool IsByWall ()
    {        
        RaycastHit2D hit = Physics2D.BoxCast(myTransform.position, characterSize, 0f, inputValues, maxRayDistance, 1 << 6);
        return hit.collider != null;
    }

    /// <summary>
    /// Sets the character as outside the exchange area.
    /// </summary>
    public void SetOutOfExchangeArea ()
    {
        onSkillExchange = null;
        isInSkillExchangeArea = false;
    }

    /// <summary>
    /// Sets the character as inside the exchange area.
    /// </summary>
    public void SetInExchangeArea (ActionCallbacks.Callback onSkillExchangeCallback)
    {
        onSkillExchange = onSkillExchangeCallback;
        isInSkillExchangeArea = true;
    }

    #endregion

    #region Skills

    /// <summary>
    /// Adds a new skill to the character.
    /// </summary>    
    public Skills SetSkill(Skills newSkill)
    {
        Skills oldSkill = skill;
        skill = newSkill;

        GetComponent<PlayerHealth>().SetNewLifeCap(skill.GetHeathValue);
        skillIconTransition.TriggerIconChange(skill.SkillUiIcon);

        return oldSkill;
    }

    /// <summary>
    /// Call the animation and effects of an slash attack.
    /// </summary>
    public void PerformeSlash (GameObject slashEffect, int damage, float effectSpawnDist)
    {
        isInputLocked = true;

        Vector3 dir;

        switch (facingDir)
        {
            case FacingDirection.Up:
                dir = Vector2.up;
                break;

            case FacingDirection.Right:
                dir = Vector2.right;
                break;

            case FacingDirection.Down:
                dir = Vector2.down;
                break;

            case FacingDirection.Left:
                dir = Vector2.left;
                break;

            default:
                dir = Vector2.right;
                break;
        }
        
        inputValues = Vector2.zero;
        Vector3 spawnPoint = myTransform.position + dir * effectSpawnDist;
        Quaternion lookDirection = Quaternion.FromToRotation(myTransform.up, dir);

        playerAnim.PlayAttackAnimation(facingDir);
        GameObject spawnedEffect = Instantiate(slashEffect, spawnPoint, lookDirection);
        spawnedEffect.GetComponent<WeaponDamageSensor>().DamageValue = damage;        
    }

    /// <summary>
    /// Call the animation and effects of the gun attack.
    /// </summary>
    public void Shoot (GameObject projectilePrefab, int damage, float effectSpawnDist)
    {
        isInputLocked = true;

        Vector3 dir;

        switch (facingDir)
        {
            case FacingDirection.Up:
                dir = Vector2.up;
                break;

            case FacingDirection.Right:
                dir = Vector2.right;
                break;

            case FacingDirection.Down:
                dir = Vector2.down;
                break;

            case FacingDirection.Left:
                dir = Vector2.left;
                break;

            default:
                dir = Vector2.right;
                break;
        }

        inputValues = Vector2.zero;
        Vector3 spawnPoint = myTransform.position + dir * effectSpawnDist;        

        playerAnim.PlayAttackAnimation(facingDir);
        GameObject spawnedEffect = Instantiate(projectilePrefab, spawnPoint, Quaternion.identity);
        spawnedEffect.GetComponent<Projectile>().SetTarget(dir);
        spawnedEffect.GetComponent<WeaponDamageSensor>().DamageValue = damage;
    }

    /// <summary>
    /// Changes the actual speed value.
    /// </summary>    
    public void ApplyDashSpeed (float boostValue, int damage, GameObject dashEffect, Sprite upwardDashSprite)
    {
        isInputLocked = true;
        isInvincible = true;
        playerAnim.SetDashAnimationState(true);
        actualSpeed = boostValue;
        
        GameObject spawnedEffect = Instantiate(dashEffect, myTransform);
        spawnedEffect.GetComponent<WeaponDamageSensor>().DamageValue = damage;
        ParticleSystemRenderer pRenderer = spawnedEffect.GetComponent<ParticleSystemRenderer>();

        if (facingDir == FacingDirection.Up)
        {
            pRenderer.material.SetTexture("_MainTex", upwardDashSprite.texture);
        }

        if (myTransform.localScale.x < 0f)
        {                                    
            pRenderer.flip = new Vector3(1f, 0f, 0f);
        }        
    }

    /// <summary>
    /// Returs the actual speed to normal movement speed.
    /// </summary>
    public void ReturnToNormalSpeed ()
    {
        isInputLocked = false;
        isInvincible = false;
        inputValues = previousInputs;
        playerAnim.SetMovingValues(inputValues, previousInputs, facingDir);
        Flip();
        playerAnim.SetDashAnimationState(false);
        actualSpeed = moveSpeed;
    }

    #endregion

    /// Event called when the character died.
    private void OnDeath ()
    {
        deathAudio.Play();
        isInputLocked = true;
        playerAnim.PlayDeathAnimation();
        GetComponent<Collider2D>().enabled = false;

        GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>().CallGameOver(myTransform.position);

        enabled = false;
    }

    /// <summary>
    /// Unlocks the character controller.
    /// </summary>
    public void UnlockController ()
    {
        if (!GetComponent<PlayerHealth>().IsDead())
        {
            isInputLocked = false;
            inputValues = previousInputs;
            SetFacingDirection(inputValues, previousInputs);
            playerAnim.SetMovingValues(inputValues, previousInputs, facingDir);
            Flip();
        }
    }

    /// <summary>
    /// Event called when the character is hit.
    /// </summary>
    public void OnHit ()
    {
        hitAudio.Play();
        isInputLocked = true;
        inputValues = Vector2.zero;
        playerAnim.PlayHitAnimation();
        GetComponentInChildren<CinemachineImpulseSource>().GenerateImpulse();
        DelayedCall.Invoke(UnlockController, hitFreezeTime, this);        
    }

    /// <summary>
    /// Lock the player inputs.
    /// </summary>
    public void LockInputs ()
    {
        isInputLocked = true;
        inputValues = Vector2.zero;
    }
}
