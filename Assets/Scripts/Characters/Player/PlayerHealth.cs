using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : Health
{
    [SerializeField] private GameObject[] heartIcons;                                               //!< UI heart icons.

    private const float DisabledSlotsAlpha = 0.3f;                                                  //!< Transparency value of disabled heart slots.

    /// <summary>
    /// Applies damage to the actual health.
    /// </summary>  
    public override void ApplyDamage (float damage)
    {
        PlayerController playerController = GetComponent<PlayerController>();

        if (!playerController.IsInvincible)
        {   
            base.ApplyDamage(damage);

            if (!IsDead())
            {
                playerController.OnHit();
            }

            heartIcons[Mathf.RoundToInt(ActualHealth)].GetComponent<UiGroupFader>().FadeOut();
        }
    }

    /// Deactivates the disabled heart slots.
    private void DeactivateHeartSlots ()
    {
        for (int i = heartIcons.Length - 1; i > ActualHealth - 1; i--)
        {
            heartIcons[i].GetComponent<Image>().color = new Color(1f, 1f, 1f, DisabledSlotsAlpha);
        }
    }

    /// Activates the active heart slots.
    private void ActivateHeartSlots ()
    {
        for (int i = 1; i < heartIcons.Length; i++)
        {
            heartIcons[i].GetComponent<Image>().color = Color.white;

            if (i > ActualHealth - 1)
            {
                heartIcons[i].GetComponent<UiGroupFader>().TurnOffInstantly();
            }
        }
    }

    /// <summary>
    /// Sets the character's life cap.
    /// </summary>
    public void SetNewLifeCap (int lifeCap)
    {
        if (lifeCap < initialHealth)
        {
            ReduceHealthCapacity(lifeCap);
            DeactivateHeartSlots();
        }
        else if (lifeCap > initialHealth)
        {
            IncreaseHealthCapacity(lifeCap);
            ActivateHeartSlots();
        }
    }

    /// <summary>
    /// Regenerates the character's health.
    /// </summary>    
    public override void RecoverLife (float lifeAmount)
    {
        if (!IsFullHeath())
        {
            base.RecoverLife(lifeAmount);
            heartIcons[Mathf.RoundToInt(ActualHealth - 1)].GetComponent<UiGroupFader>().FadeIn();
        }
    }
}
