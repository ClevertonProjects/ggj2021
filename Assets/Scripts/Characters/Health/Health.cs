﻿using UnityEngine;

/*! \class Health
 *  \brief Controls character's health.
 */
public class Health : MonoBehaviour
{
    [SerializeField] protected float initialHealth;                             //!< Initial's health amount.
    private float actualHealth;                                                 //!< Actual character's health value.
    private float previousHealthProportion;                                     //!< Previous health proportion value.

    public delegate void OnDeath();
    private OnDeath onDeath;                                                    //!< Event called when the life reaches zero.
       
    public float ActualHealth
    {
        get { return actualHealth; }
    }

    /// <summary>
    /// Check if the character's health value is zero.
    /// </summary>    
    public bool IsDead ()
    {
        return actualHealth <= 0f;
    }

    public float InitialHealth
    {
        get { return initialHealth; }
    }

    /// <summary>
    /// Check if the health if full.
    /// </summary>    
    public bool IsFullHeath ()
    {
        return actualHealth == initialHealth;
    }

    /// <summary>
    /// Sets the actual health.
    /// </summary>
    public void SetInitialHealth()
    {
        actualHealth = initialHealth;        
    }

    /// <summary>
    /// Sets the actual health.
    /// </summary>
    public void SetInitialHealth (OnDeath deathCallback = null)
    {
        SetInitialHealth();
        onDeath = deathCallback;
    }

    /// <summary>
    /// Applies damage to the actual health.
    /// </summary>    
    public virtual void ApplyDamage (float damage)
    {
        if (actualHealth > 0f)
        {
            actualHealth -= damage;

            if (IsDead())
            {
                if (onDeath != null)
                {
                    onDeath();
                }
            }
        }
    }

    /// <summary>
    /// Recovers the life value.
    /// </summary>    
    public virtual void RecoverLife (float lifeAmount)
    {
        actualHealth = Mathf.Min(actualHealth + lifeAmount, initialHealth);
    }

    /// <summary>
    /// Reduces the health capacity.
    /// </summary>
    public void ReduceHealthCapacity (int newMaxHealth)
    {
        previousHealthProportion = actualHealth / initialHealth;
        initialHealth = newMaxHealth;
        actualHealth = newMaxHealth;        
    }    

    /// <summary>
    /// Increases the health capacity.
    /// </summary>    
    public void IncreaseHealthCapacity (int newMaxHealth)
    {
        actualHealth = previousHealthProportion * newMaxHealth;
        initialHealth = newMaxHealth;
    }
}
