public class EnemyHealth : Health
{
    public override void ApplyDamage(float damage)
    {
        Enemy enemy = GetComponent<Enemy>();        
        
        base.ApplyDamage(damage);

        if (!IsDead())
        {
            enemy.OnHit();
        }
    }
}
