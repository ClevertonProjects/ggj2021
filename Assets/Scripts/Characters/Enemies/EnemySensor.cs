using UnityEngine;

public class EnemySensor : MonoBehaviour
{
    /// <summary>
    /// Check if the player is inside the agent's sight.
    /// </summary>    
    public bool IsPlayerInSight (Vector3 center, EnemyParameters parameters)
    {
        Collider2D playerCollider = Physics2D.OverlapCircle(center, parameters.SensorCheckDistance, parameters.SensorLayers);

        if (playerCollider != null)
        {            
            if (parameters.CanSeeThrough)
            {
                return true;
            }

            Vector3 direction = (playerCollider.transform.position - center).normalized;

            if (!Physics2D.Raycast(center, direction, parameters.SensorCheckDistance, parameters.ObstacleLayers))
            {
                return true;
            }
        }

        return false;
    }
}
