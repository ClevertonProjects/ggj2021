using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public EnemyParameters parameters;                                              //!< Enemy configuration parameters.
    private Vector3 initialPosition;                                                //!< Agent starting position.

    private bool isHit;                                                             //!< Check if the agent is in hit state.

    private float timer;                                                            //!< Timer used to countdowns.
    private float attackAnticipationTimer;                                          //!< Counts the time before an attack.                                                 
    [SerializeField] private Transform target;                                      //!< Agent's target object.

    private EnemyState actualState;                                                 //!< Actual behavior state.
    
    private EnemyAnimation anim;                                                    //!< Agent's animation controller.
    private EnemySensor sensor;                                                     //!< Agent sight sensor.
    private NavMeshAgent agent;                                                     //!< Reference to the agent's NavMeshAgent component.
    private Transform myTransform;                                                  //!< Reference to the agent's Transform component.   

    [SerializeField] private float PursuitUpdateDelay = 1f;                         //!< Pursuit position update delay.
    [SerializeField] private AudioSource hitAudio;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<EnemyHealth>().SetInitialHealth(OnDeath); 
        anim = GetComponentInChildren<EnemyAnimation>();
        sensor = GetComponent<EnemySensor>();
        myTransform = transform;
        agent = GetComponent<NavMeshAgent>();
        
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        agent.speed = parameters.MoveSpeed;

        actualState = EnemyState.Idle;
        initialPosition = myTransform.position;

        StartCoroutine(SensorCheckLoop());
    }

    /// Flips the transform accordingly to the moving direction.
    private void Flip (Vector3 targetPos)
    {
        float dir = Mathf.Sign(targetPos.x);        
        myTransform.localScale = new Vector3(dir, 1f, 1f);        
    }

    /// Choose random positions to move to.
    private void OnIdle ()
    {        
        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            agent.isStopped = true;
            anim.PlayIdle();

            if (parameters.CanPatrol)
            {
                if (timer <= 0f)
                {
                    Vector3 randomDirection = new Vector3(Mathf.Cos(Random.Range(-1, 1)), Mathf.Sin(Random.Range(-1, 1)), 0f);
                    Vector3 randomPosition = initialPosition + randomDirection * Random.Range(parameters.MinPatrolRadius, parameters.MaxPatrolRadius);

                    agent.SetDestination(randomPosition);
                    Flip(randomPosition - myTransform.position);
                    anim.PlayWalkAnim();
                    agent.isStopped = false;

                    timer = parameters.IdleDuration;
                }
                else
                {
                    timer -= Time.deltaTime;
                }
            }
        }
    }

    /// Chooses a position towards the player.
    private void OnPursuit ()
    {
        if (timer <= 0f)
        {
            Vector3 dir = target.position - myTransform.position;
            Flip(dir);
            agent.SetDestination(target.position - dir.normalized * parameters.AttackStoppingDist);
            timer = PursuitUpdateDelay;
        }
        else
        {
            timer -= Time.deltaTime;
        }

        float dist = (target.position - myTransform.position).sqrMagnitude;

        if (dist > parameters.SensorCheckDistance * parameters.SensorCheckDistance)
        {
            actualState = EnemyState.Idle;
            timer = 0f;            

            if (!parameters.CanPatrol)
            {
                agent.SetDestination(initialPosition);
                Vector3 dir = initialPosition - myTransform.position;
                Flip(dir);
            }

            StartCoroutine(SensorCheckLoop());
        }
        else if (dist <= parameters.AttackRange * parameters.AttackRange)
        {
            if (parameters.AttackPrefab != null)
            {
                actualState = EnemyState.Attack;
                anim.PlayIdle();
                timer = 0f;
                attackAnticipationTimer = parameters.AttackDelay;
            }
            else if (timer <= 0f)
            {
                if (target.GetComponent<Health>().IsDead())
                {
                    actualState = EnemyState.Idle;
                    return;
                }

                Vector3 randomDirection = myTransform.position + new Vector3(Random.Range(-parameters.MaxPatrolRadius, parameters.MaxPatrolRadius), Random.Range(-parameters.MaxPatrolRadius, parameters.MaxPatrolRadius), 0f);
                NavMeshHit hit;
                NavMesh.SamplePosition(randomDirection, out hit, parameters.MaxPatrolRadius, 1);
                Vector3 finalPosition = hit.position;

                agent.SetDestination(finalPosition);
                Vector3 dir = finalPosition - myTransform.position;
                Flip(dir);

                timer = parameters.AttackCooldown;
            }
        }
    }

    /// Applies damage to the player.
    private void OnAttack ()
    {        
        Vector3 dir = target.position - myTransform.position;
        Flip(dir);

        if (timer <= 0f)
        {
            if (target.GetComponent<Health>().IsDead())
            {
                actualState = EnemyState.Idle;
                return;
            }

            if (attackAnticipationTimer <= 0f)
            {
                if (dir.sqrMagnitude <= parameters.AttackRange * parameters.AttackRange)
                {                    
                    anim.PlayAttackAnim();

                    dir.Normalize();
                    Vector3 spawnPoint = myTransform.position + dir * parameters.AttackSpawnDist;
                    Quaternion lookDirection = Quaternion.FromToRotation(myTransform.up, dir);

                    if (parameters.IsProjectile)
                    {
                        GameObject projectile = Instantiate(parameters.AttackPrefab, spawnPoint, Quaternion.identity);
                        projectile.GetComponent<Projectile>().SetTarget(dir);
                    }
                    else
                    {
                        Instantiate(parameters.AttackPrefab, spawnPoint, lookDirection);
                    }

                    timer = parameters.AttackCooldown;
                    attackAnticipationTimer = parameters.AttackDelay;                    
                }
                else
                {
                    anim.PlayWalkAnim();
                    actualState = EnemyState.Pursuit;
                }
            }
            else
            {
                attackAnticipationTimer -= Time.deltaTime;
            }
        }
        else
        {
            if (dir.sqrMagnitude > parameters.AttackRange * parameters.AttackRange)
            {
                timer = 0f;

                anim.PlayWalkAnim();
                actualState = EnemyState.Pursuit;
            }

            timer -= Time.deltaTime;
        }                
    }

    /// Selects the actual state action.
    private void ProcessState ()
    {
        if (isHit) return;

        switch (actualState)
        {
            case EnemyState.Idle:
                OnIdle();
                break;

            case EnemyState.Pursuit:
                OnPursuit();
                break;

            case EnemyState.Attack:
                OnAttack();
                break;

            default:
                break;
        }
    }    

    /// Loop routine to check if the player is on sight.
    private IEnumerator SensorCheckLoop ()
    {
        WaitForSeconds delay = new WaitForSeconds(parameters.SensorCheckDelay);

        while (actualState != EnemyState.Pursuit)
        {
            yield return delay;            

            if (sensor.IsPlayerInSight(myTransform.position, parameters))
            {
                timer = 0f;
                agent.isStopped = false;
                anim.PlayWalkAnim();
                actualState = EnemyState.Pursuit;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {        
        ProcessState();        
    }

    /// Event called when the hit state ends.
    private void OnHitEnd ()
    {
        if (!GetComponent<EnemyHealth>().IsDead())
        {
            agent.isStopped = false;
            isHit = false;
        }
    }

    /// <summary>
    /// Event called when the agent is hit.
    /// </summary>
    public void OnHit()
    {
        hitAudio.Play();
        isHit = true;
        agent.isStopped = true;
        anim.PlayHitAnimation();
        DelayedCall.Invoke(OnHitEnd, parameters.HitDuration, this);        
    }

    /// Event called when the character died.
    private void OnDeath()
    {
        anim.PlayDeadAnimation();        
        agent.enabled = false;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<FadingDespawn>().TriggerObjectDespawn();
        enabled = false;
    }
}
