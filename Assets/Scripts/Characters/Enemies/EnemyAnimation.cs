using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;                                         //!< Agent's Animator controller.

    private readonly int IsMoving = Animator.StringToHash("IsMoving");              //!< Hash of the animator's isMoving parameter.
    private readonly int IsAttacking = Animator.StringToHash("IsAttacking");        //!< Hash of animator's isAttacking parameter.
    private readonly int IsHit = Animator.StringToHash("IsHit");                    //!< Hash of animator's isHit parameter.
    private readonly int IsDead = Animator.StringToHash("IsDead");                  //!< Hash of animator's isDead parameter.

    /// <summary>
    /// Sets the walking animation on.
    /// </summary>    
    public void PlayWalkAnim ()
    {
        anim.SetBool(IsMoving, true);
    }

    /// <summary>
    /// Sets the idle animation on.
    /// </summary>
    public void PlayIdle ()
    {
        anim.SetBool(IsAttacking, false);   
        anim.SetBool(IsMoving, false);
    }

    /// <summary>
    /// Sets the attack animation on.
    /// </summary>
    public void PlayAttackAnim ()
    {
        anim.SetBool(IsAttacking, true);
    }    

    /// <summary>
    /// Sets the hit animation on.
    /// </summary>
    public void PlayHitAnimation ()
    {
        anim.SetTrigger(IsHit);
    }

    /// <summary>
    /// Sets the dead animation on.
    /// </summary>
    public void PlayDeadAnimation ()
    {
        anim.SetTrigger(IsDead);
    }
}
