using System.Collections;
using UnityEngine;
using Cinemachine;

public class BossBehavior : MonoBehaviour
{
    [SerializeField] private float combatBeginDelay;                                            //< Delay time to select the first boss action.
    [SerializeField] private float minDecisionTime;                                             //!< Minimum time to select an action.
    [SerializeField] private float maxDecisionTime;                                             //!< Maximum time to select an action.
    private int actualDecisionIndex;                                                            //!< Index of the actual decision value.
    private int[] actionDecisionMatrix;                                                         //!< Values used to select a random action.

    [SerializeField] private GameObject rewardPrefab;                                           //!< End game reward prefab.

    [SerializeField] private float combatLevelTwoThreshold;                                     //!< Threshold to enter in combat level two.    
    [SerializeField] private BossAnimation anim;                                                //!< Boss animation controller.
    

    [Header("Shooting behavior")]

    [SerializeField] private float delayBeforeSpawn;                                            //!< Delay time to start spawning the projectiles.
    [SerializeField] private float delayBetweenSpawn;                                           //!< Delay time between bullets spawn.
    [SerializeField] private int[] projectilesPerLevel;                                         //!< Amount of projectiles to spawn accordingly to the combat level.

    [SerializeField] private Vector2 bulletBottomLeftTargetArea;                                //!< Minimum coordinates of the area to randomize bullets target position.
    [SerializeField] private Vector2 bulletTopRightTargetArea;                                  //!< Maximum coordinates of the area to randomize bullets target position.    

    [SerializeField] private Transform cannonPoint;                                             //!< Projectile effects spawning point.
    [SerializeField] private GameObject bulletEffectPrefab;                                     //!< Bullet behavior feedback effect.
    [SerializeField] private GameObject bulletDropPrefab;                                       //!< Bullet projectile prefab.

    [Header("Jump behavior")]

    [SerializeField] private Transform bossSpriteTransform;                                     //!< Boss sprite Transform component.    
    [SerializeField] private Transform fallDamageTransform;                                     //!< Point where the damage is applied after fall.
    [SerializeField] private GameObject fallDamageSensor;                                       //!< Sensor that applies the fall damage.
    [SerializeField] private float jumpAnticipationDuration;                                    //!< Time to wait before jump.
    [SerializeField] private float jumpBeginDuration;                                           //!< Ascending duration.
    [SerializeField] private float jumpFloatingDuration;                                        //!< Amount of time in the air.
    [SerializeField] private float jumpMovementSpeed;                                           //!< Movement speed until in the air.
    [SerializeField] private float jumpFallDuration;                                            //!< Time duration before fall from jump.
    [SerializeField] private Vector3 jumpMaxHeight;                                             //!< Maximum sprite height position.
    [SerializeField] private Vector3 jumpLandingHeight;                                         //!< Height position to land.
    [SerializeField] private AnimationCurve movementCurve;                                      //!< Movement variation per time.

    [SerializeField] private CinemachineImpulseSource lightScreenShake;                         //!< Screen shake generation.
    [SerializeField] private CinemachineImpulseSource strongScreenShake;                        //!< Screen shake generation.

    [SerializeField] private AudioSource chargeAudio;
    [SerializeField] private AudioSource landAudio;

    private void Start()
    {
        GetComponentInChildren<BossHealth>().SetInitialHealth(OnDeath);
        actionDecisionMatrix = IndexRandomizer.GenerateRandomIndexes(10, 11);
    }

    /// Calculates the combat level, based in the boss life amount.
    private int GetCombatLevel ()
    {
        Health health = GetComponentInChildren<Health>();
        float lifeProportion = health.ActualHealth / health.InitialHealth;
        int level = 0;

        if (lifeProportion < combatLevelTwoThreshold)
        {
            level = 1;
        }

        return level;
    }

    /// Moves the object until it reaches the final position.
    private IEnumerator JumpMovingLoop ()
    {
        bossSpriteTransform.GetComponent<Collider2D>().enabled = false;

        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        Transform myTransform = transform;
        Transform playerTransform = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        Vector3 direction;
        float proportion = 0f;
        float elapsedTime = 0f;

        anim.PlayJumpAnticipationAnim();        
        lightScreenShake.GenerateImpulse();

        yield return new WaitForSeconds(jumpAnticipationDuration);        

        fallDamageSensor.SetActive(false);
        chargeAudio.Play();
        anim.PlayJumpAnim();

        while (proportion < 1f)
        {
            bossSpriteTransform.localPosition = Vector3.Lerp(jumpLandingHeight, jumpMaxHeight, movementCurve.Evaluate(proportion));

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / jumpBeginDuration;
        }

        elapsedTime = 0f;
        anim.PlayFallAnim();

        while (elapsedTime < jumpFloatingDuration)
        {
            direction = (playerTransform.position - fallDamageTransform.position).normalized;
            myTransform.position += direction * jumpMovementSpeed * Time.deltaTime;

            yield return delay;

            elapsedTime += Time.deltaTime;            
        }
        
        elapsedTime = 0f;
        proportion = 0f;
        Vector3 from = bossSpriteTransform.localPosition;

        while (proportion < 1f)
        {
            bossSpriteTransform.localPosition = Vector3.Lerp(from, jumpLandingHeight, movementCurve.Evaluate(proportion));

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / jumpFallDuration;
        }

        landAudio.Play();
        bossSpriteTransform.localPosition = jumpLandingHeight;

        strongScreenShake.GenerateImpulse();
        fallDamageSensor.SetActive(true);
        yield return new WaitForSeconds(1f);        
        anim.PlayIdleAnimation();
        bossSpriteTransform.GetComponent<Collider2D>().enabled = true;

        float decisionTime = Random.Range(minDecisionTime, maxDecisionTime);
        DelayedCall.Invoke(SelectAction, decisionTime, this);
    }

    /// Spawns the boss projectiles.
    private IEnumerator ShootingLoop ()
    {
        bossSpriteTransform.GetComponent<Collider2D>().enabled = false;
        anim.PlayShootingAnimation();
        yield return new WaitForSeconds(delayBeforeSpawn);

        WaitForSeconds delay = new WaitForSeconds(delayBetweenSpawn);
        int amount = projectilesPerLevel[GetCombatLevel()];
        int spawnedAmount = 0;
        GameObject spawnedObject;

        while (spawnedAmount < amount)
        {
            spawnedObject = Instantiate(bulletEffectPrefab, cannonPoint.position, cannonPoint.rotation);
            spawnedObject.GetComponent<Projectile>().SetTarget(-cannonPoint.up);
            spawnedAmount++;
            yield return delay;
        }

        anim.PlayIdleAnimation();
        bossSpriteTransform.GetComponent<Collider2D>().enabled = true;
        spawnedAmount = 0;
        
        Vector3 selectedLandPosition;

        while (spawnedAmount < amount)
        {
            spawnedObject = Instantiate(bulletDropPrefab, fallDamageTransform.position, Quaternion.identity);
            spawnedAmount++;

            if (spawnedAmount == 1)
            {
                Transform playerTransform = GameObject.FindGameObjectWithTag(Tags.Player).transform;
                selectedLandPosition = playerTransform.position;
            }
            else
            {
                selectedLandPosition = new Vector3(Random.Range(bulletBottomLeftTargetArea.x, bulletTopRightTargetArea.x),
                                                    Random.Range(bulletBottomLeftTargetArea.y, bulletTopRightTargetArea.y), 0f);
            }

            spawnedObject.GetComponent<BulletDrop>().MoveToPosition(selectedLandPosition);
            yield return delay;
        }        

        float decisionTime = Random.Range(minDecisionTime, maxDecisionTime);
        DelayedCall.Invoke(SelectAction, decisionTime, this);
    }

    /// Executes the jump action behavior.
    public void RunJumpAction()
    {
        StartCoroutine(JumpMovingLoop());
    }

    /// Executes the shooting action behavior.
    public void RunShootingAction()
    {
        StartCoroutine(ShootingLoop());
    }

    /// Selects an action.
    private void SelectAction ()
    {
        int action = actionDecisionMatrix[actualDecisionIndex] % 2 == 0 ? 0 : 1;

        if (action == 0)
        {
            RunShootingAction();
        }
        else
        {
            RunJumpAction();
        }

        actualDecisionIndex++;

        if (actualDecisionIndex >= actionDecisionMatrix.Length)
        {
            actualDecisionIndex = 0;
        }
    }

    public void StartCombat ()
    {
        DelayedCall.Invoke(SelectAction, combatBeginDelay, this);
    }

    /// Triggers the end game events.
    private void OnDeath ()
    {
        StopAllCoroutines();
        anim.PlayDeadAnimation();
        strongScreenShake.GenerateImpulse();

        fallDamageSensor.SetActive(false);
        GetComponent<FadingDespawn>().TriggerObjectDespawn();

        Instantiate(rewardPrefab, transform.position, Quaternion.identity);
    }    
}
