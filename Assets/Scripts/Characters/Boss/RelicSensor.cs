using UnityEngine;

public class RelicSensor : MonoBehaviour
{
    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            collision.GetComponent<PlayerController>().LockInputs();
            collision.enabled = false;
            GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>().CallGameEnd();
            Destroy(gameObject);
        }
    }
}
