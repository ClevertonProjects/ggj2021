using UnityEngine;
using UnityEngine.Playables;

public class BossCombatTrigger : MonoBehaviour
{
    [SerializeField] private GameObject bossObject;                                 //!< Boss game object.
    [SerializeField] private PlayableDirector intro;                                //!< Intro animation controller.

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            collision.GetComponent<PlayerController>().LockInputs();
            intro.Play();
        }
    }

    /// <summary>
    /// Event called when the intro animation ends.
    /// </summary>
    public void OnIntroEnd ()
    {
        bossObject.SetActive(true);
        intro.gameObject.SetActive(false);
        GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>().UnlockController();
        GameObject.FindGameObjectWithTag(Tags.EnemiesContainer).SetActive(false);
        bossObject.GetComponent<BossBehavior>().StartCombat();

        gameObject.SetActive(false);
    }
}
