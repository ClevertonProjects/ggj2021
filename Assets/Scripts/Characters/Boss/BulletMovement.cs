using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed;                                       //!< Object's moving speed.    
    [SerializeField] private GameObject explosionPrefab;                            //!< Explosion effect prefab.    

    private Transform myTransform;                                                  //!< Transform component.

    // Start is called before the first frame update
    void Start()
    {        
        myTransform = transform;
    }

    /// Moves the object.
    private void Move ()
    {
        myTransform.localPosition += Vector3.down * moveSpeed * Time.deltaTime;

        if (myTransform.localPosition.y <= 0f)
        {
            Instantiate(explosionPrefab, myTransform.position, Quaternion.identity);
            GetComponentInParent<BulletDrop>().DestroyObject();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
}
