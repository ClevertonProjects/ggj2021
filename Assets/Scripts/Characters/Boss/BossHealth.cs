using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : Health
{
    /// <summary>
    /// Applies damage to the actual health.
    /// </summary>  
    public override void ApplyDamage(float damage)
    {
        base.ApplyDamage(damage);
        GetComponent<SpriteBlinkEffect>().StartFlashing();        
    }
}
