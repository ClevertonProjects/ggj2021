using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimation : MonoBehaviour
{
    [SerializeField] private Animator anim;                                                     //!< Agent's Animator controller.

    private readonly int IsReadyToJump = Animator.StringToHash("IsReadyToJump");                //!< Hash of the animator's IsReadyToJump parameter.
    private readonly int IsJumping = Animator.StringToHash("IsJumping");                        //!< Hash of animator's IsJumping parameter.
    private readonly int IsShooting = Animator.StringToHash("IsShooting");                      //!< Hash of animator's IsShooting parameter.
    private readonly int IsFalling = Animator.StringToHash("IsFalling");                        //!< Hash of animator's IsFalling parameter.
    private readonly int IsDead = Animator.StringToHash("IsDead");                              //!< Hash of animator's isDead parameter.

    /// <summary>
    /// Sets the pre jump animation on.
    /// </summary>    
    public void PlayJumpAnticipationAnim()
    {
        anim.SetBool(IsReadyToJump, true);
    }

    /// <summary>
    /// Sets the jump animation on.
    /// </summary>
    public void PlayJumpAnim ()
    {
        anim.SetBool(IsReadyToJump, false);
        anim.SetBool(IsJumping, true);
    }

    /// <summary>
    /// Sets the falling animation on.
    /// </summary>
    public void PlayFallAnim ()
    {
        anim.SetBool(IsJumping, false);
        anim.SetBool(IsFalling, true);
    }

    /// <summary>
    /// Sets the idle animation on.
    /// </summary>
    public void PlayIdleAnimation ()
    {
        anim.SetBool(IsFalling, false);
        anim.SetBool(IsShooting, false);
    }

    /// <summary>
    /// Sets the shooting animation on.
    /// </summary>
    public void PlayShootingAnimation()
    {
        anim.SetBool(IsShooting, true);
    }

    /// <summary>
    /// Sets the dead animation on.
    /// </summary>
    public void PlayDeadAnimation ()
    {
        anim.SetTrigger(IsDead);
    }
}
