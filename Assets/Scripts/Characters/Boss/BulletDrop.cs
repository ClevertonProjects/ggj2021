using System.Collections;
using UnityEngine;

public class BulletDrop : MonoBehaviour
{
    [SerializeField] private GameObject bulletSprite;                                                   //!< Bullet sprite object.
    [SerializeField] private float movementDuration;                                                    //!< Movement duration until it reaches the final position.    
    [SerializeField] private AnimationCurve movementCurve;                                              //!< Movement variation per time.

    /// Moves the object until it reaches the final position.
    private IEnumerator MovingLoop (Vector3 to)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        Transform myTransform = transform;
        Vector3 from = myTransform.position;
        float proportion = 0f;
        float elapsedTime = 0f;

        while (proportion < 1f)
        {
            myTransform.position = Vector3.Lerp(from, to, movementCurve.Evaluate(proportion));

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / movementDuration;
        }

        bulletSprite.SetActive(true);
    }

    /// <summary>
    /// Sets the object positions config.
    /// </summary>    
    public void MoveToPosition (Vector3 to)
    {
        StartCoroutine(MovingLoop(to));
    }

    /// <summary>
    /// Destroy the object.
    /// </summary>
    public void DestroyObject ()
    {
        Destroy(gameObject);
    }
}
