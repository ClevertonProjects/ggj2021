﻿using UnityEngine;
using UnityEngine.Events;

/*! \class WeaponDamageSensor
 *  \brief Sensor that acts on weapon collisions.
 */
public class WeaponDamageSensor : MonoBehaviour
{
    [SerializeField] private bool destroyOnCollision;                                   //!< Check if the object is destroy when it detects a collision.
    [SerializeField] private float damageValue;                                         //!< Damage value.

    public UnityEvent OnHit;

    public float DamageValue
    {
        set { damageValue = value; }
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {        
        Health health = collision.GetComponent<Health>();

        if (health != null)
        {
            health.ApplyDamage(damageValue);

            OnHit.Invoke();
        }
        
        if (destroyOnCollision)
        {
            Destroy(gameObject);
        }        
    }
}
