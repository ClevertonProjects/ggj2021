﻿using UnityEngine;
using UnityEngine.Audio;

/*! \class MusicPlayer
    *  \brief Controls the scenes music.
    */
public class MusicPlayer : MonoBehaviour
{
    public static bool IsCreated { get; private set; }                          //!< Safe check used to create only one instance of this object.
        
    [SerializeField] private AudioSource musicAudio;                            //!< Scene music audio source.
    [SerializeField] private AudioMixer musicMixer;                             //!< Audio mixer that controls the music volume.
    [SerializeField] private AudioMixerSnapshot[] snapshots;                    //!< Mixer volume states.

    [SerializeField] private float transitionDuration;                          //!< Snapshots transition time duration.        

    private const float MusicPlayDelay = 0.4f;                                  //!< Delay time to start playing the background music.

    private void Awake()
    {
        if (!IsCreated)
        {
            IsCreated = true;
            DontDestroyOnLoad(gameObject);

            DelayedCall.Invoke(PlayMusic, MusicPlayDelay, this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /// Applies a snapshot transition.
    private void ApplySnapshotTransition(AudioMixerSnapshot[] selectedSnapshot, float[] weights, float transitionDuration)
    {
        musicMixer.TransitionToSnapshots(selectedSnapshot, weights, transitionDuration);
    }

    /// <summary>
    /// Play the scene music.
    /// </summary>
    public void PlayMusic()
    {            
        musicAudio.Play();
    }
}