using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ActiveIconTransition : MonoBehaviour
{
    private const float TransitionDuration = 1f;                                        //!< Icon's transition duration.

    /// Skill icon transition loop.
    private IEnumerator TransitionLoop (Sprite skillIcon)
    {
        Image imgSkillIcon = GetComponent<Image>();
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float proportion = 0f;
        float elapsedTime = 0f;

        while (proportion < 1f)
        {
            imgSkillIcon.fillAmount = Mathf.Lerp(1f, 0f, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / TransitionDuration;
        }

        imgSkillIcon.sprite = skillIcon;
        proportion = 0f;
        elapsedTime = 0f;

        while (proportion < 1f)
        {
            imgSkillIcon.fillAmount = Mathf.Lerp(0f, 1f, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / TransitionDuration;
        }
    }

    /// <summary>
    /// Starts the transition icon effect.
    /// </summary>
    public void TriggerIconChange (Sprite newIcon)
    {
        StartCoroutine(TransitionLoop(newIcon));
    }
}
