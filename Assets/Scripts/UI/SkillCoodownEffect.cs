using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SkillCoodownEffect : MonoBehaviour
{
    [SerializeField] private Image imgSkillPanelBlock;                                          //!< Skill cooldown panel block.
    
    /// Counts the cooldown effect.
    private IEnumerator CooldownLoop (float cooldownValue)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float proportion = 0f;
        float elapsedTime = 0f;

        while (proportion < 1f)
        {
            imgSkillPanelBlock.fillAmount = Mathf.Lerp(1f, 0f, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / cooldownValue;
        }

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Starts the cooldown UI effect.
    /// </summary>
    public void TriggerCooldown (float cooldownValue)
    {
        imgSkillPanelBlock.fillAmount = 1f;
        gameObject.SetActive(true);
        
        StartCoroutine(CooldownLoop(cooldownValue));
    }
}
