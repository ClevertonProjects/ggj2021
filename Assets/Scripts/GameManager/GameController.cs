using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject endScreenPrefab;                                  //!< Game end screen.
    [SerializeField] private GameObject corpsePrefab;                           //!< Character's death point corpse prefab.

    private const float EndFadingDelay = 2f;                                    //!< Delay time to call for game over screen fade.

    private void Start()
    {
        string path = Application.persistentDataPath + "/" + LocalFilesNames.GameConfig;

        if (LocalFileHandler.HasSave(path))
        {
            PlayerData pData = LocalFileHandler.Load<PlayerData>(path);
            SpawnCorpse(pData.DeathPoint);
            LocalFileHandler.DeleteSave(path);
        }
    }

    /// Spawn the character's corpse.
    private void SpawnCorpse (Vector2 deathPosition)
    {        
        Instantiate(corpsePrefab, deathPosition, Quaternion.identity);        
    }

    /// Triggers the scene fading.
    private void TriggerSceneFade ()
    {
        SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
        fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToGameScene);
    }

    /// Triggers the game transition to menu.
    private void TransitionToMenu ()
    {
        SceneFader fader = GameObject.FindGameObjectWithTag(Tags.Fader).GetComponent<SceneFader>();
        fader.FadeScene(FadeState.FadeIn, SceneLoading.GoToMenuScene);
    }

    /// <summary>
    /// Calls the game end sequence.
    /// </summary>
    public void CallGameEnd ()
    {
        GameObject endScreen = Instantiate(endScreenPrefab);
        endScreen.GetComponent<UiGroupFader>().FadeIn();

        DelayedCall.Invoke(TransitionToMenu, 5f, this);
    }

    /// <summary>
    /// Calls the game over sequence.
    /// </summary>
    public void CallGameOver (Vector2 deathPosition)
    {
        PlayerData pData;
        pData.DeathPoint = deathPosition;

        string path = Application.persistentDataPath + "/" + LocalFilesNames.GameConfig;
        LocalFileHandler.Save(path, pData);

        DelayedCall.Invoke(TriggerSceneFade, EndFadingDelay, this);                
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame ()
    {
        Application.Quit();
    }
}
