﻿using System.Collections.Generic;
using UnityEngine;

/*! \class IndexRandomizer
 *  \brief Handles the generation of random int values.
 */
public class IndexRandomizer
{
    /// <summary>
    /// Returns an array with random int values.
    /// </summary>
    public static int[] GenerateRandomIndexes (int pickAmount, int range)
    {
        int selectedIndex;
        int[] indexes = new int[pickAmount];
        List<int> availableIndexes = new List<int>(range);

        // Fill the list with all indexes.
        for (int i = 0; i < range; i++)
        {
            availableIndexes.Add(i);
        }

        // Select randomly the indexes to use.
        for (int i = 0; i < pickAmount; i++)
        {
            selectedIndex = Random.Range(0, availableIndexes.Count);
            indexes[i] = availableIndexes[selectedIndex];

            availableIndexes.RemoveAt(selectedIndex);
        }

        return indexes;
    }
}
