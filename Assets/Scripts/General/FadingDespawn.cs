using System.Collections;
using UnityEngine;

public class FadingDespawn : MonoBehaviour
{
    [SerializeField] private SpriteRenderer characterSprite;                                //!< Character's sprite to fade.

    private const float FadeoutDuration = 1f;                                               //!< Fading duration time.
    private const float DespawnTime = 5f;                                                   //!< Time to wait before fade the object out.

    /// Despawn sequence loop.
    private IEnumerator DespawnLoop ()
    {
        yield return new WaitForSeconds(DespawnTime);

        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float proportion = 0f;
        float elapsedTime = 0f;

        while (proportion < 1f)
        {
            characterSprite.color = Color.Lerp(Color.white, Color.clear, proportion);

            yield return delay;

            elapsedTime += Time.deltaTime;
            proportion = elapsedTime / FadeoutDuration;
        }

        Destroy(gameObject);
    }

    /// <summary>
    /// Starts the despawn sequence.
    /// </summary>
    public void TriggerObjectDespawn ()
    {
        StartCoroutine(DespawnLoop());
    }
}
