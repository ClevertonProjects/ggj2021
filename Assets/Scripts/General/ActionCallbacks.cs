﻿/*! \class ActionCallbacks
 *  \brief Has the game callback types.
 */
public class ActionCallbacks
{
    public delegate void Callback();                                        //!< Simple void callback.
}
