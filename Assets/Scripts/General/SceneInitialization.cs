﻿using UnityEngine;
using UnityEngine.Events;

/*! \class SceneInitialization
 *  \brief Has the initial actions when a scene loads.
 */
public class SceneInitialization : MonoBehaviour
{
    public GameObject FaderPrefab;                                                              //!< Fader object prefab.    
    public UnityEvent OnInitialFadeComplete;                                                    //!< Event called when the scene completes the initial fade out effect.

    // Start is called before the first frame update
    void Awake()
    {
        CreateSceneFader();        
    }

    /// Callback of the fade effect.
    private void OnFadeEnd()
    {
        OnInitialFadeComplete.Invoke();
    }

    /// Creates the scene fader and triggers the first fade out.
    private void CreateSceneFader()
    {
        if (FaderPrefab == null) return;

        GameObject fader = Instantiate(FaderPrefab);
        fader.GetComponent<SceneFader>().FadeScene(FadeState.FadeOut, OnFadeEnd);
    }    
}
