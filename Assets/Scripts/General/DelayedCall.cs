using System.Collections;
using UnityEngine;

/*! \class DelayedCall
    *  \brief Used to call a method after a delay time.
    */
public class DelayedCall
{
    public delegate void SimpleCallback();                              //!< A void callback without parameters.          
                
    /// Routine that manages the delayed method call.
    private static IEnumerator DelayRotine (float delayTime, SimpleCallback callback)
    {
        WaitForSeconds delay = new WaitForSeconds(delayTime);

        yield return delay;

        callback();
    }        

    /// <summary>
    /// Invokes a method after a delay time.
    /// </summary>        
    public static void Invoke (SimpleCallback callback, float delayTime, MonoBehaviour runner)
    {
        runner.StartCoroutine(DelayRotine(delayTime, callback));
    }            
}

