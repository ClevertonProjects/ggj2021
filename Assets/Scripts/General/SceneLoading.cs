﻿using UnityEngine.SceneManagement;

/*! \class SceneLoading
 *  \brief Controls all the unity scenes loading.
 */
public class SceneLoading
{
    private const int IndexMenu = 0;                                    //!< Build index of Menu scene.   
    private const int IndexTutorial = 1;                                //!< Build index of Tutorial scene.
    private const int IndexGame = 2;                                    //!< Build index of Game scene.    

    /// <summary>
    /// Loads the menu scene.
    /// </summary>
    public static void GoToMenuScene()
    {
        SceneManager.LoadScene(IndexMenu);
    }    

    /// <summary>
    /// Loads the tutorial scene.
    /// </summary>
    public static void GoToTutorialScene ()
    {
        SceneManager.LoadScene(IndexTutorial);
    }

    /// <summary>
    /// Loads the game scene.
    /// </summary>
    public static void GoToGameScene()
    {
        SceneManager.LoadScene(IndexGame);
    }
}
