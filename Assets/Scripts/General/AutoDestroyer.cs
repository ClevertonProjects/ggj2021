using UnityEngine;

public class AutoDestroyer : MonoBehaviour
{
    [SerializeField] private float timeActive;                      //!< Time before destroying the object.

    private void Start()
    {
        Destroy(gameObject, timeActive);
    }
}
