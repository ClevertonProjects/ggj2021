﻿using System.Collections;
using UnityEngine;

/*! \class PanelGrow
 *  \brief Animates an UI panel size transition.
 */
public class PanelSizeAnim : MonoBehaviour
{
    [SerializeField] private float duration;                                            //!< Size change duration.
    [SerializeField] private AnimationCurve growAnimation;                              //!< Time variation curve for grow effect.
    [SerializeField] private AnimationCurve shrinkAnimation;                            //!< Time variation curve for srink effect.
    [SerializeField] private Vector2 growSize;                                          //!< Rect size for grow state.
    [SerializeField] private Vector2 shrinkSize;                                        //!< Rect size for shrink state.
    [SerializeField] private bool isGrowFullscreen;                                     //!< Check if the panel will overlaps the entire screen.

    public delegate void OnSizeChangeEnd();

    /// Panel size variation loop.
    private IEnumerator SizeVariationLoop(Vector2 toSize, float duration, AnimationCurve durationCurve, OnSizeChangeEnd onSizeChangeEnd)
    {
        RectTransform panelRect = GetComponent<RectTransform>();
        Vector2 fromSize = panelRect.sizeDelta;        

        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float t = 0f;
        float elapsedTime = 0f;
        float progression = 0f;

        while (progression < 1f)
        {
            t = durationCurve.Evaluate(progression);
            panelRect.sizeDelta = Vector2.Lerp(fromSize, toSize, t);            

            yield return delay;

            elapsedTime += Time.deltaTime;
            progression = elapsedTime / duration;
        }

        panelRect.sizeDelta = toSize;

        if (onSizeChangeEnd != null)
        {
            onSizeChangeEnd();
        }
    }

    /// <summary>
    /// Increase an UI panel size.
    /// </summary>
    public void Grow()
    {
        Grow(null);
    }

    /// <summary>
    /// Increase an UI panel size.
    /// </summary>
    public void Grow(OnSizeChangeEnd sizeChangeCallback)
    {
        Vector2 selectedSize = growSize;

        if (isGrowFullscreen)
        {
            selectedSize = new Vector2(Screen.width, Screen.height);
        }

        StartCoroutine(SizeVariationLoop(selectedSize, duration, growAnimation, sizeChangeCallback));
    }

    /// <summary>
    /// Increase an UI panel size isntantly.
    /// </summary>
    public void GrowInstantly()
    {
        Vector2 selectedSize = growSize;

        if (isGrowFullscreen)
        {
            selectedSize = new Vector2(Screen.width, Screen.height);
        }

        GetComponent<RectTransform>().sizeDelta = selectedSize;
    }

    /// <summary>
    /// Decrease an UI panel size.
    /// </summary>
    public void Shrink()
    {
        Shrink(null);
    }

    /// <summary>
    /// Decrease an UI panel size.
    /// </summary>
    public void Shrink(OnSizeChangeEnd sizeChangeCallback)
    {
        StartCoroutine(SizeVariationLoop(shrinkSize, duration, shrinkAnimation, sizeChangeCallback));
    }

    /// <summary>
    /// Decrease the UI panel size instantly.
    /// </summary>
    public void ShrinkInstantly()
    {
        GetComponent<RectTransform>().sizeDelta = shrinkSize;
    }
}
