using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBlinkEffect : MonoBehaviour
{
    public float TimeOn = 0.1f;                                             //!< Object's time turned on during flash.
    public float TimeOff = 0.1f;                                            //!< Object's time turned off during flash.
    public float Duration = 1.0f;                                           //!< Flash effect duration.

    [SerializeField] private Material normalMat;
    [SerializeField] private Material blinkMat;

    private bool isFlashing;                                                //!< Check if the object is flashing.
    private bool isOn;                                                      //!< Check if the actual object flash state is on.    

    private SpriteRenderer sr;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private IEnumerator BlinkLoop ()
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        float flashTimer = 0f;
        float durationTimer = 0f;

        while (isFlashing)
        {
            // Alternate the object renderer's materials, during the flash duration.        
            if (flashTimer >= TimeOn)
            {
                isOn = !isOn;
                
                if (isOn)
                {
                    sr.material = normalMat;
                }
                else
                {
                    sr.material = blinkMat;
                }

                flashTimer = 0f;
            }

            yield return delay;

            flashTimer += Time.deltaTime;
            durationTimer += Time.deltaTime;

            if (durationTimer >= Duration)
            {
                isFlashing = false;
                sr.material = normalMat;
            }
        }
    }

    /// <summary>
    /// Starts the blinking effect.
    /// </summary>
    public void StartFlashing ()
    {
        StopAllCoroutines();
        isFlashing = true;
        StartCoroutine(BlinkLoop());
    }
}
