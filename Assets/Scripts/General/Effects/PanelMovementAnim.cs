﻿using System.Collections;
using UnityEngine;

/*! \class PanelMovementAnim
 *  \brief Controls an UI panel movement animation on x axis.
 */
public class PanelMovementAnim : MonoBehaviour
{    
    [SerializeField] private AnimationCurve movementCurve;                              //!< Animation curve controlling the panel movement.
    [SerializeField] private float movementDuration;                                    //!< Panel's movement duration.    
    [SerializeField] private CanvasGroup canvasGroup;                                   //!< Painel's canvas group.
   
    public delegate void OnMovementEnd();                                               //!< Callback called when the panel movement ends.    

    [SerializeField] private bool moveFullScreen;                                       //!< Check if the panel will move until the end of screen.
    [SerializeField] private Vector2 baseFrom;                                          //!< Base position to move from.
    [SerializeField] private Vector2 baseTo;                                            //!< Base position to move to.
    private const int DefaultDirection = -1;                                            //!< Default movement direction.    
     
    /// Movement loop routine.
    private IEnumerator MovementRoutine (Vector2 toPosition, float toAlpha, RectTransform panelRect, OnMovementEnd onMovementEnd)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        
        Vector2 from = panelRect.anchoredPosition;
        float fromAlpha = canvasGroup.alpha;
        
        float elapsedTime = 0f;
        float proportion = 0f;
        float t = 0f;

        while (proportion < 1f)
        {
            t = movementCurve.Evaluate(proportion);
            panelRect.anchoredPosition = Vector2.Lerp(from, toPosition, t);
            canvasGroup.alpha = Mathf.Lerp(fromAlpha, toAlpha, t);

            yield return delay;

            elapsedTime += Time.smoothDeltaTime;
            proportion = elapsedTime / movementDuration;
        }

        panelRect.anchoredPosition = toPosition;        
        canvasGroup.alpha = toAlpha;

        if (onMovementEnd != null)
        {
            onMovementEnd();
        }
    }

    /// Gets the panel position based on screen size.
    private Vector2 GetScreenPosition (int direction)
    {
        if (moveFullScreen)
        {
            return new Vector2(Screen.width, 0f) * -direction;
        }
        else
        {
            return baseFrom * -direction;
        }
    }

    /// <summary>
    /// Triggers the panel animation to enter on screen, with a specific moving direction.
    /// </summary>
    public void MoveIn (int direction, OnMovementEnd movementEndCallback)
    {        
        StopAllCoroutines();        

        RectTransform panelRect = GetComponent<RectTransform>();
        panelRect.anchoredPosition = GetScreenPosition(direction);

        StartCoroutine(MovementRoutine(baseTo, 1f, panelRect, movementEndCallback));
        canvasGroup.blocksRaycasts = true;
    }

    /// <summary>
    /// Triggers the panel animation to enter on screen, from the actual position.
    /// </summary>    
    public void MoveIn (OnMovementEnd movementEndCallback)
    {        
        StopAllCoroutines();        

        RectTransform panelRect = GetComponent<RectTransform>();     
        StartCoroutine(MovementRoutine(baseTo, 1f, panelRect, movementEndCallback));
        canvasGroup.blocksRaycasts = true;
    }

    /// <summary>
    /// Triggers the panel animation to enter on screen, from the actual position.
    /// </summary>    
    public void MoveIn()
    {
        MoveIn(null);
    }

    /// <summary>
    /// Triggers the panel animation to exit the screen.
    /// </summary>
    public void MoveOut (int direction, OnMovementEnd movementEndCallback)
    {        
        StopAllCoroutines();        

        RectTransform panelRect = GetComponent<RectTransform>();
        Vector2 toPos = GetScreenPosition(-direction);

        StartCoroutine(MovementRoutine(toPos, 0f, panelRect, movementEndCallback));
        canvasGroup.blocksRaycasts = false;
    }

    /// <summary>
    /// Triggers the panel animation to exit the screen.
    /// </summary>
    public void MoveOut (OnMovementEnd movementEndCallback)
    {
        MoveOut(-DefaultDirection, movementEndCallback);
    }

    /// <summary>
    /// Triggers the panel animation to exit the screen.
    /// </summary>
    public void MoveOut()
    {
        MoveOut(null);
    }
}
