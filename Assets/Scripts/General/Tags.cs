﻿public class Tags
{   
    public const string Fader = "SceneFader";
    public const string GameController = "GameController";    
    public const string Player = "Player";
    public const string EnemiesContainer = "EnemiesContainer";
}
