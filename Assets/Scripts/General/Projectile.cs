﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed;                                               //!< projectile's movement speed.    

    private Vector3 moveDir;                                                            //!< Moving direction.
    private Transform myTransform;                                                      //!< Transform component.    
    
    private const float MaxTimeAlive = 2f;                                              //!< Maximum amount of time for the projectile to still alive.

    private void Start ()
    {        
        DelayedCall.Invoke(DestroyObject, MaxTimeAlive, this);
        myTransform = transform;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }    

    /// Increments the character position.
    private void Move()
    {
        myTransform.position += moveDir * speed * Time.deltaTime;        
    }    

    /// <summary>
    /// Set the projectile direction to move.
    /// </summary>
    public void SetTarget (Vector2 direction)
    {
        moveDir = direction;
    }    

    /// <summary>
    /// Destroys this object
    /// </summary>
    public void DestroyObject ()
    {
        Destroy(gameObject);
    }
}
