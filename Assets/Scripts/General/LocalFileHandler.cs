﻿using UnityEngine;
using System.IO;

/*! \class LocalFileHandler
 *  \brief Handles the local saving and loading data.
 */
public class LocalFileHandler
{
    /// <summary>
    /// Check if a saved file exists.
    /// </summary>    
    public static bool HasSave (string path)
    {
        return File.Exists(path);
    }

    /// <summary>
    /// Saves the data in a local file.
    /// </summary>
    public static void Save<T> (string path, T config)
    {
        string saveContent = JsonUtility.ToJson(config);
        File.WriteAllText(path, saveContent);
    }

    /// <summary>
    /// Load the local file data.
    /// </summary>
    public static T Load<T> (string path)
    {   
        if (File.Exists(path))
        {
            string content = File.ReadAllText(path);
            T configData = JsonUtility.FromJson<T>(content);

            return configData;
        }      
        else
        {
            return default;
        }
    }

    /// <summary>
    /// Deletes a save file.
    /// </summary>    
    public static void DeleteSave (string path)
    {
        File.Delete(path);
    }
}
