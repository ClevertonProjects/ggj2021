using UnityEngine;

public class HealthRegen : MonoBehaviour
{
    [SerializeField] private int RegenAmount;                                   //!< Amount of life to regenerate.
    [SerializeField] private GameObject collectEffectPrefab;                    //!< Collect feedback effect prefab.

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.CompareTag(Tags.Player))
        {
            collision.GetComponent<PlayerHealth>().RecoverLife(RegenAmount);
            Instantiate(collectEffectPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
