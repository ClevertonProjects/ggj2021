﻿/*! \class LocalFilesNames
 *  \brief Has the name of all game files.
 */
public class LocalFilesNames
{
    public const string GameConfig = "GameConfig";                           //!< Game configuration's file name.    
}
