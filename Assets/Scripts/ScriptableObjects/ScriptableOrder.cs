public class ScriptableOrder
{
    public const int SkillDash = 0;                                 //!< Dash skill menu order.
    public const int SkillSword = 1;                                //!< Sword skill menu order.
    public const int SkillGun = 2;                                  //!< Gun skill menu order.
    public const int EnemyParameters = 5;                           //!< Enemy parameters menu order.
}
