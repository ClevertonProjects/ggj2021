using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Custom/Enemy Parameters", order = ScriptableOrder.EnemyParameters)]
public class EnemyParameters : ScriptableObject
{
    public float MoveSpeed;                                                 //!< Agent moving speed.

    public float AttackDelay;                                               //!< Delay time before an attack is executed.
    public float AttackStoppingDist;                                        //!< Attack stopping distance from the player's character.
    public float AttackRange;                                               //!< Attack's range.
    public float AttackCooldown;                                            //!< Attack's cooldown.    
    public float AttackSpawnDist;                                           //!< Distance to spawn the attack prefab.
    public bool IsProjectile;                                               //!< Check if the attack type is a projectile.
    public GameObject AttackPrefab;                                         //!< Prefab to create on attacks.

    public bool CanPatrol;                                                  //!< Check if the agent is allowed to patrol.
    public bool CanSeeThrough;                                              //!< Check if the agent sight is affected by walls.
    public float MinPatrolRadius;                                           //!< Minimum radius to get a position to move to.
    public float MaxPatrolRadius;                                           //!< Maximum radius to get a position to move to.
    public float IdleDuration;                                              //!< Time duration in idle state.
    public float HitDuration;                                               //!< Amount of time in hit state.

    public float SensorCheckDelay;                                          //!< Delay time to check if the player is on sight.
    public float SensorCheckDistance;                                       //!< Raycast sensor distance.
    public LayerMask SensorLayers;                                          //!< Layer to get the player's proximity.
    public LayerMask ObstacleLayers;                                        //!< Layers to get collisions from sensor.
}
