﻿using UnityEngine;

/*! \struct PlayerData
    *  \brief Used to descerialize the player data.
    */
[System.Serializable]
public struct PlayerData
{
    public Vector2 DeathPoint;                           //!< Position where the character died.
}
